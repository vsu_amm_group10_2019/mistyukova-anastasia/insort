﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace insort3b
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int num = (int)numericUpDown1.Value;
            Record[] array = new Record[num];
            for (int i = 0; i < num; i++)
            {
                array[i] = new Record();
                array[i].Key = random.Next(0, 20);
                array[i].Number = i;
            }
            ArrayToGrid(array);
            Sort(array);
            MessageBox.Show("Done!");
        }
        private void ArrayToGrid(Record[] array)
        {
            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = array.Length;
            dataGridView1.ClearSelection();
            for (int i = 0; i < array.Length; i++)
            {
                dataGridView1.Rows[0].Cells[i].Value = array[i].Key;
            }
            dataGridView1.Refresh();
            Thread.Sleep(500);
        }

        private void Swap(Record[] array, int i, int j)
        {
            Record tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
            ArrayToGrid(array);
            dataGridView1.Rows[0].Cells[i].Style.BackColor = System.Drawing.Color.White;
            dataGridView1.Rows[0].Cells[j].Style.BackColor = System.Drawing.Color.White;
        }

        private Record[] Sort(Record[] array)
        {
            int size = array.Length;
            for (int i = size / 2 - 1; i >= 0; i--)
            {
                ArrayToTree(array, treeView1);
                ArrayToHeap(array, i, size);
            }
            for (int i = size - 1; i >= 0; i--)
            {
                dataGridView1.Rows[0].Cells[0].Style.BackColor = System.Drawing.Color.Red;
                dataGridView1.Rows[0].Cells[i].Style.BackColor = System.Drawing.Color.Green;
                dataGridView1.Refresh();
                Thread.Sleep(500);
                Swap(array, 0, i);
                ArrayToTree(array, treeView1);
                ArrayToHeap(array, 0, i);
            }
            return array;
        }

        private void ArrayToHeap(Record[] array, int index, int size)
        {
            int largest = index;
            int left = 2 * index + 1;
            int right = 2 * index + 2;
            if (left < size && array[largest].Key < array[left].Key)
            {
                largest = left;
            }
            if (right < size && array[largest].Key < array[right].Key)
            {
                largest = right;
            }
            if (largest != index)
            {
                dataGridView1.Rows[0].Cells[largest].Style.BackColor = System.Drawing.Color.Red;
                dataGridView1.Rows[0].Cells[index].Style.BackColor = System.Drawing.Color.Green;
                dataGridView1.Refresh();
                Thread.Sleep(500);
                Swap(array, largest, index);
                ArrayToTree(array, treeView1);
                ArrayToHeap(array, largest, size);
            }
        }

        private void SubTree(Record[] array, int index, TreeNode treeNode)
        {
            int size = array.Length;
            if (index < size / 2)
            {
                int left = 2 * index + 1;
                int right = 2 * index + 2;
                if (left < size)
                {
                    TreeNode ln = new TreeNode(array[left].Key.ToString());
                    SubTree(array, left, ln);
                    treeNode.Nodes.Add(ln);
                }
                if (right < size)
                {
                    TreeNode rn = new TreeNode(array[right].Key.ToString());
                    SubTree(array, right, rn);
                    treeNode.Nodes.Add(rn);
                }
            }
        }

        private void ArrayToTree(Record[] array, TreeView tw)
        {
            tw.Nodes.Clear();
            TreeNode n = new TreeNode(array[0].Key.ToString());
            SubTree(array, 0, n);
            tw.Nodes.Add(n);
            tw.ExpandAll();
            tw.Refresh();
            Thread.Sleep(500);
        }
    }
}
